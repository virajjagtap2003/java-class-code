// By normal method
import java.util.*;
class Search{

	int Binary(int []arr,int no){
		int start=0;
		int end=arr.length-1;

		while(start<=end){
			int mid=(start+end)/2;
			if(arr[mid]==no){
				return mid;
			}
			else if(arr[mid]<no){
				start=mid+1;
			}
			else{
				end=mid-1;
			}
		}
		return -1;
	}
	public static void main(String[] args){
		int arr[]=new int[]{2,3,7,9,10,22,44}; 
		System.out.println("Enter element to search");
		Scanner sc=new Scanner(System.in);
		int no=sc.nextInt();

		Search obj=new Search();
		int index=obj.Binary(arr,no);
		
		System.out.println("Element found at "+index);
		
	}
}
