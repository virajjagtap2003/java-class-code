// By recurrsion
import java.util.*;

class Binary{
		int Search(int []arr,int Start,int End,int no){
			if(Start>End){
				return -1;
			}
			else{
				int mid=(Start+End)/2;

				if(arr[mid]==no){
					return mid;
				}
				if(no<arr[mid]){
					return Search(arr,Start,mid-1,no);
				}
				else{
					return Search(arr,mid+1,End,no);
				}
			}
		}

	public static void main(String[] args){
		int arr[]=new int[]{2,4,8,10,11,15,17,44};
		Binary obj=new Binary();
		System.out.println("enter element to search");
		Scanner sc=new Scanner(System.in);
		int no=sc.nextInt();
		int index=obj.Search(arr,0,arr.length-1,no);
		System.out.println("Element found at index  "+index);
	}
}
