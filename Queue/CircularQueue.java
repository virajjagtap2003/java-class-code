import java.util.*;
class Circular{
	int Queuearr[];                           // 10 20 30 40 pop 10 pop 20 add 50 add 60  == 30 40 50 60 
	int front;
	int rear;
	int maxSize;
	
	Circular(int size){
		this.Queuearr=new int[size];
		this.front=-1;
		this.rear=-1;
		this.maxSize=size;
	}
	void enqueue(int data){
		if((front==0 && rear==maxSize-1) || ((rear+1)%maxSize==front)){
			System.out.println("Queue is full");
			return;
		}else if(front==-1){
			front=rear=0;
		}else if(rear==maxSize-1 && front!=0){
			rear=0;

		}else{
			rear++;
		}
		Queuearr[rear]=data;
	}
	int dequeue(){
		if(front==-1){
			System.out.println("Queue is empty");
			return -1;
		}else{
			int ret=Queuearr[front];
			if(front==rear){
				rear=front=-1;
			}else if(front==maxSize-1){
				front=0;
			}else{
				front++;
			}
			return ret;

		}
	}
	void PrintQueue(){
		if(front<=rear){
			for(int i=front;i<=rear;i++){
				System.out.print(Queuearr[i]+"  ");
			}
			System.out.println();
		}
		else{
			for(int i= front;i<maxSize;i++){
				System.out.print(Queuearr[i]+"  ");
			}
			
			for(int i=0;i<=rear;i++){
				System.out.print(Queuearr[i]+"  ");
			}
			System.out.println();
		}
	}

}
class Client{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter array size");
		int size=sc.nextInt();
		Circular cu=new Circular(size);

		char ch;

		do{
			System.out.println("1.enqueue");
			System.out.println("2.dequeue");
			System.out.println("3.Printqueue");

			System.out.println("enter your choice");
			int choice=sc.nextInt();

			switch(choice){
				case 1:{
					System.out.println("Enter data to add");
					int data =sc.nextInt();
					cu.enqueue(data);

				}
				break;
				case 2:{
					int ret=cu.dequeue();
					if(ret!=-1){
						System.out.println(ret+"  Popped");
					}
				}
				break;
				case 3:{
					cu.PrintQueue();

				}
				break;
				default:
					System.out.println("Wrong choice");
			}	
			System.out.println("Do you want to continue");
			ch=sc.next().charAt(0);

			
		}while(ch=='Y' || ch=='y');
	}
}
