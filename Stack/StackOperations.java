import java.util.*;
class Methods{
	public static void main(String[] args){
		Stack<Integer> s=new Stack<Integer>();

		s.push(10);
		s.push(20);
		s.push(30);
		s.push(40);
		s.push(50);

		
		System.out.println(s);// [10,20,30,40,50]
		System.out.println(s.size());
		System.out.println(s.pop()); // 50

		System.out.println(s.peek()); // 40

		s.pop();
		s.pop();
		s.pop();
		s.pop();
		 
		System.out.println(s);  // []
		s.pop(); // Stackempty exception



	}
}
