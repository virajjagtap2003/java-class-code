// By using array

import java.util.*;
class StackDemo{

	int maxSize;
	int Stackarr[];
	int top=-1;
	StackDemo(int size){
		this.maxSize=size;
		this.Stackarr=new int[size];
	}
	void push(int data){
		if(top==maxSize-1){
			
			System.out.println("Stack overflow");
			return;
		}else{
			top++;
			Stackarr[top]=data;
		}

	}
	boolean empty(){
		if(top== -1){
			return true;
		}
		else{
			return false;
		}
	}
	int pop(){
		if(empty()){
			System.out.println("Stack empty");
			return -1;
		}else{
			int val=Stackarr[top];
			top--;
			return val;
		}
	}
	int peek(){
		if(empty()){
			System.out.println("Stack empty");
			return -1;
		}else{
			return Stackarr[top];
			
		}
	}

	int size(){
		return top;
	}
	

}

class Client{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter stack size");

		int size=sc.nextInt();
		StackDemo s=new StackDemo(size);
		char ch;

		do{
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.peek");
			System.out.println("4.empty");
			System.out.println("5.size");
			System.out.println("6.printStack");

			System.out.println("Enter your choice");
			int choice=sc.nextInt();
		
			switch(choice){
			
				case 1:{
					System.out.println("Enter element");
					int data=sc.nextInt();
					s.push(data);
				}
				case 2:{
					int flag=s.pop();
					if(flag!= -1){
						System.out.println(flag+" Poped");
					}
				}
				break;
				
				case 3:{ 
					int flag=s.peek();
					if(flag!=-1){
						System.out.println(flag);
					}
					
				}
				case 4:{
					boolean val=s.empty();
					if(val==true){
						System.out.println("Empty");
					}else{
						System.out.println("Not empty");
					}
				}
				break;

				case 5:{
					int sz=s.size();
					System.out.println("Stack size "+ (sz+1));
				}
				break;
				default:
				       System.out.println("Wrong choice");
				       break;
			}
			System.out.println("Do you want to continue");
			ch=sc.next().charAt(0);
		}while(ch=='Y' || ch=='y');
	}
}
