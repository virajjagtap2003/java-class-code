// Reverse String using stack
import java.util.*;
class Reverse{
	String Rev(String str){
		Stack<Character> st=new Stack<Character>();
		char arr[]=new char[str.length()];

		for(int i=0;i<str.length();i++){
			st.push(str.charAt(i));
		}
		System.out.println(st);
		int i=0;
		while(!st.empty()){
			arr[i]=st.pop();
			i++;
		}
		return new String(arr); // return object of string bcz to catch type is string and arr type is char so.
	}
}
class Client{
	public static void main(String[] args){
		Scanner  sc=new Scanner(System.in);
		System.out.println("Enter String to reverse");
		String str=sc.next();
		Reverse obj=new Reverse();
		String ans=obj.Rev(str);
		System.out.println(ans);

	}
}
