
class Node{
	int data;
	Node next;


	Node(int data){
		this.data=data;
	}
}
class LinkedListDemo{
	Node head=null;
	
	void AddFirst(int data){
		Node newNode=new Node(data);
		if(head==null){
			head=newNode;
		}else{
			newNode.next=head;
			head=newNode;
		}
	}
	
	void AddLast(int data){
		Node newNode=new Node(data);
		if(head==null){
			head=newNode;

		}else{
			Node temp=head;
			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next=newNode;
		}
	}
	int count(){
		int count=0;
		Node temp=head;
		while(temp!=null){
			count++;
			temp=temp.next;
		}
		return count;
	}

	void AddAtPos(int pos,int data){
		Node temp=head;
		if(pos<=0 || pos>count()+1){
			System.out.println("Wrong Input");
		}
		if(pos==1){
			AddFirst(data);
		}else if(pos==count()+1){
			AddLast(data);
		}
		else{
			Node newNode=new Node(data);
			while(pos-2!=0){
				temp=temp.next;
				pos--;
			}
			newNode.next=temp.next;
			temp.next=newNode;
		}
	}
	
 	void DeleteFirst(){

		if(head==null){
			System.out.println("Empty");
			return;
		}
		if(count()==1){
			head=null;
		}
		else{
			head=head.next;
		
		}
	}

	void DeleteLast(){
		if(head==null){
			System.out.println("Empty");

		}
		if(count()==1){
			head=null;
		}
		else{
			Node temp=head;
			while(temp.next.next!=null){
				temp=temp.next;
			}
			temp.next=null;
		}
	}

	void DeleteAtPos(int pos){
		if(pos<=0 || pos>count()){
			System.out.println("Wrong Input");

		}
		else if(pos==1){
			DeleteFirst();
		}
		else if(pos==count()){
			DeleteLast();
		}
		else{
			Node temp=head;
			while(pos-2!=0){
				temp=temp.next;
				pos--;
			}
			temp.next=temp.next.next;

		}
	}
	void PrintLL(){
		if(head==null){
			System.out.println("Empty LinkedList");
		}else{
			Node temp=head;
			while(temp!=null){
				System.out.print(temp.data+"  ");
				temp=temp.next;
			}
			System.out.println();
		}
		
	}


}
class Client{
	public static void main(String[] args){
		LinkedListDemo ll=new LinkedListDemo();
		ll.AddFirst(10);
		ll.AddFirst(20);

		ll.PrintLL();

		ll.AddLast(30);
		int count=ll.count();
		System.out.println(count);

		ll.PrintLL();

		ll.AddAtPos(3,40);
		ll.PrintLL();

		count=ll.count();
		System.out.println(count);

		ll.DeleteFirst();
		ll.PrintLL();

		ll.DeleteLast();
		ll.PrintLL();
		ll.DeleteAtPos(2);
		ll.PrintLL();
	


	}
}
