class Sort{
	int count=0;
	void Bubble(int []arr,int n){
		if(n==1){
			return;
		}
		
		int flag=0;
		for(int i=0;i<arr.length-1;i++){
			count++;
			if(arr[i]>arr[i+1]){
				flag=1;
			
				int temp=arr[i];
				arr[i]=arr[i+1];
				arr[i+1]=temp;
			}
		}
		if(flag==0){
			return;
		}
		Bubble(arr,n-1);
	}

	public static void main(String[] args){
		int arr[]=new int[]{3,2,4,1,6,8,9};
		Sort obj=new Sort();
		obj.Bubble(arr,arr.length);
		for(int no:arr){
			System.out.print(no+"  ");
		}
		System.out.println(obj.count);

	}
}
