class Insertion{
	public static void main(String[] args){
		int arr[]=new int[]{2,1,5,3,7,6};

		for(int i=1;i<arr.length;i++){
			int ele=arr[i];
			int j=i-1;

			while(j>=0 && arr[j]>ele){
				arr[j+1]=arr[j];
				j--;
			}
			arr[j+1]=ele;
		}

		for(int no:arr){
			System.out.print(no+"  ");
		}
	}
}
