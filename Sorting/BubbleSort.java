class Bubble{
	public static void main(String[] args){
		int arr[]=new int[]{3,1,2,5,8,4,9};
		int count=0;
		for(int i=0;i<arr.length;i++){
			int flag=0;
			for(int j=0;j<arr.length-1-i;j++){  // j<arr.length-1-i  avoid checking the already shifted greater elements
				count++;
				
				if(arr[j]>arr[j+1]){
					flag=1;
					int temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
				}
			}
			if(flag==0){     // flag for decreasing the count of iterations if array is already sorted
				break;
			}
		}
		for(int no:arr){
			System.out.print(no+"  ");
		}
		System.out.println();
		System.out.println(count);
	}
}
