// generate prefix sum array but not use another array 



class PrefixArray{
	static int[] SameArr(int arr[]){	
		
		for(int i=1;i<arr.length;i++){
			arr[i]=arr[i-1]+arr[i];
		}
		return arr;

	}
	public static void main(String[] args){
		int arr[]=new int[]{1,2,3,4,5};
		int ans[]=SameArr(arr);	
		for(int no:ans){
			System.out.println(no);
		}
	}
}


