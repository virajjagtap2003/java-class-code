// You have to return same array after rotating it by B times towards right 
// a=[1,2,3,4]==[4,1,2,3]  1 times
//            ==[3,4,1,2]  2 times


class Rotate{
	static int[] Rotate(int[] arr){
		for(int no:arr){
			System.out.print(no+" ");
		}
		System.out.println();
		int B=2;
		for(int i=0;i<B;i++){
			int last=arr[arr.length-1];
			for(int j=arr.length-1;j>0;j--){
				arr[j]=arr[j-1];
			}
			arr[0]=last;
		}
		return arr;
	}
	public static void main(String[] args){
		int arr[]=new int[]{1,2,3,4};
		int ans[]=Rotate(arr);
		for(int no:ans){
			System.out.print(no+" ");
		}
			
	}
}
