// print subarray pairs and also count pairs

class SubArrayDemo{
	static int Subarray(int arr[]){
		int count=0;
	
		for(int i=0;i<arr.length;i++){
			for(int j=i;j<arr.length;j++){
				for(int k=i;k<=j;k++){

					System.out.print(arr[k]+"  ");
				}
			
				
				count++;
				System.out.println();
			}
		}
		return count;
		
	}

	public static void main(String[] args){
				
		int arr[]=new int[]{1,2,3,4,5};
		int ans=Subarray(arr);
		System.out.println("count of subarray= "+ans);
		
	}
}

