// maximum subarray sum

class SubArraySumDemo{

	static int sum(int a[]){
		int sum=0;
		int maxSum=0;
		for(int i=0;i<a.length;i++){
			for(int j=i;j<a.length;j++){
				for(int k=i;k<=j;k++){
					sum=sum+a[k];
				}
				if(maxSum<sum){
					maxSum=sum;
				}
				sum=0;
			}
		}
		return maxSum;
	}
	public static void main(String[] args){
		int arr[]=new int[]{-2, -3, 4, -1, -2, 1, 5, -3};
		int ans=sum(arr);
		System.out.println(ans);
	}
}
